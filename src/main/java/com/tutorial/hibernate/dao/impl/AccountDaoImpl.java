package com.tutorial.hibernate.dao.impl;

import com.tutorial.hibernate.config.HibernateConfig;
import com.tutorial.hibernate.dao.AccountDao;
import com.tutorial.hibernate.entitites.AccountEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class AccountDaoImpl implements AccountDao {

    @Override
    public void saveAccount(AccountEntity accountEntity) {
        Transaction transaction = null;
        try (Session session = HibernateConfig.getSessionFactory().openSession()) {
            //Start a transaction
            transaction = session.beginTransaction();
            //Save a user in the database
            session.save(accountEntity);
            //Commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw new RuntimeException();
        }
    }
}
