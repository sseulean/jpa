package com.tutorial.hibernate.service;

import com.tutorial.hibernate.entitites.AccountEntity;

public interface AccountService {


    void saveAccount(AccountEntity accountEntity);

    void deleteAccount();
}