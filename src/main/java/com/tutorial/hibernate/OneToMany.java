package com.tutorial.hibernate;

import com.tutorial.hibernate.config.HibernateConfig;
import com.tutorial.hibernate.entitites.BooksEntity;
import com.tutorial.hibernate.entitites.ReviewsEntity;
import org.hibernate.Session;

import java.util.HashSet;
import java.util.Set;

public class OneToMany {

    public static void main(String args[]) {

        Session session = HibernateConfig.getSessionFactory().openSession();
        session.beginTransaction();

        BooksEntity booksEntity = new BooksEntity();
        booksEntity.setAuthor("Demo author");
        booksEntity.setTitle("This is a demo title");

        ReviewsEntity reviewsEntity = new ReviewsEntity();
        reviewsEntity.setReviewContent("This is a demo review");
        Set<ReviewsEntity> bookReviews = new HashSet<>();
        bookReviews.add(reviewsEntity);
        booksEntity.setReviews(bookReviews);

        session.save(booksEntity);

        session.getTransaction().commit();
        session.close();

    }
}
