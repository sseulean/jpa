package com.tutorial.hibernate.service.impl;

import com.tutorial.hibernate.dao.AccountDao;
import com.tutorial.hibernate.dao.impl.AccountDaoImpl;
import com.tutorial.hibernate.entitites.AccountEntity;
import com.tutorial.hibernate.service.AccountService;

public class AccountServiceImpl implements AccountService {


    private AccountDao accountDao;

    public  AccountServiceImpl(){
        this.accountDao=new AccountDaoImpl();
    }

    @Override
    public void saveAccount(AccountEntity accountEntity){
        accountDao.saveAccount(accountEntity);
    }

    @Override
    public void deleteAccount() {

    }
}
