package com.tutorial.hibernate.dao.impl;

import com.tutorial.hibernate.config.HibernateConfig;
import com.tutorial.hibernate.dao.EmployeeDao;
import com.tutorial.hibernate.entitites.EmployeeEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmployeeDaoImpl implements EmployeeDao {

    @Override
    public void saveEmployee(EmployeeEntity employeeEntity) {

        Transaction transaction = null;
        try (Session session = HibernateConfig.getSessionFactory().openSession()) {
            //Start a transaction
            transaction = session.beginTransaction();
            //Save a user in the database
            session.save(employeeEntity);
            //Commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }

            throw new RuntimeException();
        }

    }
}
