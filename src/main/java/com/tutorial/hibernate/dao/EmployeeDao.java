package com.tutorial.hibernate.dao;

import com.tutorial.hibernate.entitites.EmployeeEntity;

public interface EmployeeDao {

    void saveEmployee(EmployeeEntity employeeEntity);
}
