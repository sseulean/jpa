package com.tutorial.hibernate;

import com.tutorial.hibernate.entitites.AccountEntity;
import com.tutorial.hibernate.entitites.EmployeeEntity;
import com.tutorial.hibernate.service.AccountService;
import com.tutorial.hibernate.service.EmployeeService;
import com.tutorial.hibernate.service.impl.AccountServiceImpl;
import com.tutorial.hibernate.service.impl.EmployeeServiceImpl;

public class OneToOne {

    public static void main(String[] args) {

        AccountService accountService = new AccountServiceImpl();
        EmployeeService employeeService = new EmployeeServiceImpl();

        AccountEntity accountEntity = createTestAccountEntity();
        accountService.saveAccount(accountEntity);

        EmployeeEntity employeeEntity = createTestEmployeeEntity(accountEntity);
        employeeService.saveEmployee(employeeEntity);
    }

    private static AccountEntity createTestAccountEntity() {
        AccountEntity account = new AccountEntity();
        account.setAccountNumber("123-345-65454");
        return account;
    }

    private static EmployeeEntity createTestEmployeeEntity(AccountEntity accountEntity) {
        EmployeeEntity emp = new EmployeeEntity();
        emp.setName("demo-user");
        emp.setAccount(accountEntity);
        return emp;
    }

}
