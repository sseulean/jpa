package com.tutorial.hibernate.service.impl;

import com.tutorial.hibernate.dao.EmployeeDao;
import com.tutorial.hibernate.dao.impl.EmployeeDaoImpl;
import com.tutorial.hibernate.entitites.EmployeeEntity;
import com.tutorial.hibernate.service.EmployeeService;

public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDao employeeDao;

    public EmployeeServiceImpl(){
        this.employeeDao=new EmployeeDaoImpl();
    }

    @Override
    public void saveEmployee(EmployeeEntity employeeEntity){
        employeeDao.saveEmployee(employeeEntity);
    }
}
