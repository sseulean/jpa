package com.tutorial.hibernate.service;

import com.tutorial.hibernate.entitites.EmployeeEntity;

public interface EmployeeService {

     void saveEmployee(EmployeeEntity employeeEntity);
}
