package com.tutorial.hibernate.dao;

import com.tutorial.hibernate.entitites.AccountEntity;

public interface AccountDao {

    void saveAccount(AccountEntity accountEntity);
}
